<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MalogiqueController extends AbstractController
{
    /**
     * @Route("/malogique", name="malogique")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MalogiqueController.php',
        ]);
    }
}
