<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GestionProduitController extends AbstractController
{
    /**
     * @Route("/gestion/produit", name="gestion_produit")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/GestionProduitController.php',
        ]);
    }
}
